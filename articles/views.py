from django.shortcuts import render, render_to_response
from .models import Article, Disciplines, Lections, Month


def index(request):
    if request.method == 'GET':
        articles = Article.objects.all().order_by('-id')[:3]
        disciplines = Disciplines.objects.all()
        months = Month.objects.filter(status=True)
        context = {'articles': articles,
                   'disciplines': disciplines,
                   'months': months}
        return render(request, 'index.html', context)


def learn_more(request):
    return render_to_response('learn_more.html')


def single_article(request, pk):
    if request.method == "GET":
        single = Article.objects.get(id=pk)
        return render(request, 'single.html', {'single': single})


def single_discipline(request, pk):
    if request.method == "GET":
        disciplines = Disciplines.objects.get(id=pk)
        return render(request, 'disciplines.html', {'disciplines': disciplines})

