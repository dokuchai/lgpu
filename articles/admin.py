from django.contrib import admin
from .models import Article, Disciplines, Lections, Month


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title",)


@admin.register(Disciplines)
class DisciplinesAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Lections)
class LectionsAdmin(admin.ModelAdmin):
    list_display = ("name", 'month')


@admin.register(Month)
class MonthAdmin(admin.ModelAdmin):
    list_display = ("month",)