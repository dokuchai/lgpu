from django.db import models
from django.urls import reverse


class Article(models.Model):
    title = models.CharField('Заголовок статьи', max_length=100)
    text = models.TextField('Текст статьи')
    image = models.ImageField(upload_to='images/', blank=True, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("article", kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class Disciplines(models.Model):
    name = models.CharField('Название дисциплины', max_length=30)
    text = models.TextField('Текст дисциплины')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('discipline', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'Дисциплина'
        verbose_name_plural = 'Дисциплины'
        ordering = ['id']


class Month(models.Model):
    month = models.CharField('Месяц', max_length=8)
    status = models.BooleanField('Показать на сайте', default=False)

    def __str__(self):
        return self.month

    def lect_on_month(self):
        return Lections.objects.filter(month=self.id).order_by('data', 'time_start')

    class Meta:
        verbose_name = 'Месяц'
        verbose_name_plural = 'Месяцы'
        ordering = ['id']


class Lections(models.Model):
    month = models.ForeignKey(Month, on_delete=models.PROTECT, blank=True, null=True)
    name = models.ForeignKey(Disciplines, on_delete=models.CASCADE)
    data = models.IntegerField('Дата проведения занятия')
    time_start = models.TimeField('Время начала занятия', auto_now=False)
    time_finish = models.TimeField('Время окончания занятия', auto_now=False)

    def __str__(self):
        return self.name.name

    class Meta:
        verbose_name = 'Лекция'
        verbose_name_plural = 'Лекции'
