from django.urls import path
from .views import index, single_article, single_discipline, learn_more

urlpatterns = [
    path('index/', index, name='index'),
    path('article/<int:pk>/', single_article, name='article'),
    path('discipline/<int:pk>/', single_discipline, name='discipline'),
    path('learn-more/', learn_more, name='learn_more')
]